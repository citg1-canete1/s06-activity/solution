s06-SQL
// List the books Authored by Marjorie Green.
  SELECT title.title
  FROM title
  INNER JOIN author_title ON .title_id= author.title.title_id
  INNER JOIN author ON author_title.au_id = author.au_id
  WHERE author.au_lname = 'Green' AND author.au_fname = 'Margorie';


// List the books Authored by Michael O'Leary.
   SELECT title.title
  FROM title
  INNER JOIN author_title ON .title_id= author.title.title_id
  INNER JOIN author ON author_title.au_id = author.au_id
  WHERE author.au_lname = 'O 'Leary' AND author.au_fname = 'Michael';

// Write the author/s of the "The Busy Executive Databas Guide"
  SELECT author.au_lname, author.au_fname
  FROM author
  INNER JOIN author_title ON author.au_id = author_title.au_id
  INNER JOIN title ON author_title.title_id = title.title_id
  WHERE title.title = 'The Busy Executive's Database Guide';

// Identify the publisher of the "But Is It User Friendly?".
  SELECT publisher.pub_name
  FROM publisher
  INNER JOIN title ON publisher.pub_id = title.pub_id
  WHERE title.title = 'But Is It User Friendly?';

// List the Books publisher by Algodata Inforsystems.
  SELECT title.title
  FROM title
  INNER JOIN publisher ON title.pub_id = publisher.pub_id
  WHERE publisher.pub_name = 'Algodata Inforsystems';

 /*Create SQL Syntax and Queries to create a database based on the provided ERD for a blog post.*/

    run -- mysql -u root -p
    CREATE DATABASE blog_db;
    USE blog_db;

 /*users
 id INT
 emal VARCHAR(100)
 password VARCHAR(300)
 datetime_created DATETIME */

	 	CREATE TABLE users (
	  id INT PRIMARY KEY AUTO_INCREMENT,
	  email VARCHAR(100) NOT NULL,
	  password VARCHAR(300) NOT NULL,
	  datetime_created DATETIME NOT NULL
	);
--Show Tables;

 /*posts
 id INT
 author_id INT
 title VARCHAR(500)
 content VARCHAR(5000)
 datetime_poster DATETIME */

 		CREATE TABLE posts (
  id INT PRIMARY KEY AUTO_INCREMENT,
  author_id INT NOT NULL,
  title VARCHAR(500) NOT NULL,
  content VARCHAR(5000) NOT NULL,
  datetime_posted DATETIME NOT NULL,
  FOREIGN KEY (author_id) REFERENCES users(id)
);
--show tables;

/*post_comments
 id INT
 post_id INT
 user_id INT
 content VARCHAR(5000)
 datetime_commented DATETIME*/

 		CREATE TABLE post_comments (
  id INT PRIMARY KEY AUTO_INCREMENT,
  post_id INT NOT NULL,
  user_id INT NOT NULL,
  content VARCHAR(5000) NOT NULL,
  datetime_commented DATETIME NOT NULL,
  FOREIGN KEY (post_id) REFERENCES posts(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);
--show tables;

 /*post_likes
 id INT
 post_id INT
 user_id INT
 datetime_liked DATETIME*/

 		CREATE TABLE post_likes (
  id INT PRIMARY KEY AUTO_INCREMENT,
  post_id INT NOT NULL,
  user_id INT NOT NULL,
  datetime_liked DATETIME NOT NULL,
  FOREIGN KEY (post_id) REFERENCES posts(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

--show tables;


